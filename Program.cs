using System;
using System.Security.Cryptography;
using System.Text;
using System.Threading;

namespace Encryption
{
    class Program
    {
        static void Main(string[] args)
        {
            string check = string.Empty;
            Console.WriteLine("******* Encryption *******");            
            do
            {
                Console.WriteLine((Environment.NewLine));
                Console.WriteLine(" Note : The encryption key should be either 128 or 192 bit of data\n (16 charector make 128 data)");
                Console.WriteLine(" E -> Encryption text\n D -> Decryption text\n X -> Exit");
                check=Console.ReadLine();

                if (check == "E")
                {
                    encryptString();
                }
                else if (check == "D")
                {
                    decrptString();
                }
                else
                {
                    Console.WriteLine("Please make sure that the given options are correct..");
                }
            }
            while (check !="x");
            Thread.Sleep(1000);
            Console.WriteLine("Thanks");
            
            
        }

        private static void decrptString()
        {
            try
            {
                Console.WriteLine("Text.");
                string _encryptText = Console.ReadLine();
                Console.WriteLine("Encryption key.");
                string _cypherText = Console.ReadLine();

                byte[] inputArray = Convert.FromBase64String(_encryptText);
                TripleDESCryptoServiceProvider tripleDES = new TripleDESCryptoServiceProvider();
                tripleDES.Key = UTF8Encoding.UTF8.GetBytes(_cypherText);
                tripleDES.Mode = CipherMode.ECB;
                tripleDES.Padding = PaddingMode.PKCS7;
                ICryptoTransform cTransform = tripleDES.CreateDecryptor();
                byte[] resultArray = cTransform.TransformFinalBlock(inputArray, 0, inputArray.Length);
                tripleDES.Clear();
                Console.WriteLine(" The Decrypted String : " + UTF8Encoding.UTF8.GetString(resultArray));

            }
            catch { }
        }

        private static void encryptString()
        {
            try
            {

                Console.WriteLine(" Text.");
                string _encryptText = Console.ReadLine();
                Console.WriteLine(" Encryption key.");
                string _cypherText = Console.ReadLine();
                byte[] inputArray = UTF8Encoding.UTF8.GetBytes(_encryptText);
                TripleDESCryptoServiceProvider tripleDES = new TripleDESCryptoServiceProvider();
                tripleDES.Key = UTF8Encoding.UTF8.GetBytes(_cypherText);
                tripleDES.Mode = CipherMode.ECB;
                tripleDES.Padding = PaddingMode.PKCS7;
                ICryptoTransform cTransform = tripleDES.CreateEncryptor();
                byte[] resultArray = cTransform.TransformFinalBlock(inputArray, 0, inputArray.Length);
                tripleDES.Clear();
                Console.WriteLine(" The Decrypted String : " + Convert.ToBase64String(resultArray, 0, resultArray.Length));
            }
            catch { }
        }
    }
}
